#ifndef _mymalloc_h
#define _mymalloc_h
#define malloc(x) mymalloc(x, __FILE__, __LINE__)
#define free(x) myfree(x, __FILE__, __LINE__)
#endif

static char myblock[4096];

void* mymalloc(size_t, const char * file, int line);
void myfree(void* ptr, const char* file, int line);
