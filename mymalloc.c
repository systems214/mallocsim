#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "mymalloc.h"

//get k bits from b->e then bitmask last 8 bits with 0xFF (8 bit hex)
unsigned char bitmask(unsigned short size, short b, short e){
	return (size<<(16-e)>>(16-e+b))&0xFF;
}

void mk_md(int arr_ind, size_t size, int toggle){
	//give it the array index, the allocation size in size_t, and 0 if it
	//is unallocated, 1 if it is allocated
	myblock[arr_ind] = bitmask((unsigned int) size, 0, 8);
	myblock[arr_ind+1] = bitmask((unsigned int) size, 8, 12)<<4;
	myblock[arr_ind+1] |= toggle;
}

unsigned short get_size(int arr_ind){
	return (((unsigned char) myblock[arr_ind+1]>>4<<8) | ((unsigned char) myblock[arr_ind]));
	//returns the size of the user memory in bytes as an short
}

int get_toggle(int arr_ind){
	return myblock[arr_ind+1]&0x1;
	//returns 1 if it is allocated
	//returns 0 if it is unallocated
}

void* mymalloc(size_t size, const char* file, int line){
	//metadata is two bytes (two indexes)
	unsigned short user_size = (unsigned short) size;
	unsigned short md_size = 0;
	unsigned int md_toggle = 0;
	char * ptr = myblock+2;
	unsigned int index = 2;



	if( (myblock[0] != 238) && (myblock[1] != 113)){
		memset(myblock, '\0', 4096);
		myblock[0] = (unsigned char) 238;
		myblock[1] = (unsigned char) 113;
		myblock[2] = (unsigned char) 252;
		myblock[3] = (unsigned char) 240;
	}

	if(user_size <= 0) {
		fprintf(stderr, "Error: invalid size\n");
		return NULL;
	}

	while(index < 4094) {
		md_size = get_size(index);
		md_toggle = get_toggle(index);

		if(md_toggle == 0) {
			if(md_size > (user_size+2)) {
				mk_md(index + 2 + user_size, (size_t) md_size-(2+user_size), 0);
				mk_md(index, (size_t)  user_size, 1);

				return (void *) ptr + 2;
			}
			else if(md_size == user_size) {
				mk_md(index, (size_t) user_size, 1);
				return (void *) ptr + 2;
			}
		}
		index = index + (2 + md_size);
		ptr = ptr + (2 + md_size);
	}

	return NULL;
}

void myprint() {
	unsigned int index = 2;
	unsigned short md_size = 0;
	unsigned int md_toggle = 0;
	while(index < 4095) {
		md_size = get_size(index);
		md_toggle = get_toggle(index);
		printf("At Index %d:\n\tThe size is %hi\n\tThe toggle is %d\n", index, md_size, md_toggle);
		index = index + get_size(index)+2;
	}
}

void myfree(void* ptr, const char* file, int line) {
	unsigned long long beginning = (unsigned long long) &myblock[2];
	unsigned long long end =  beginning + 4094;
	unsigned long long currentAddress = 0;
	unsigned int index = 2;
	unsigned long long our_ptr = (unsigned long long) ptr;
	unsigned int md_toggle = 2;
	unsigned short md_size = 0;
	unsigned int current_index = 2;
	unsigned int next_index = 0;
	unsigned int md_toggle_n = 2;
	unsigned short md_size_n = 0;
	unsigned int counter = 0;



	if( (myblock[0] != 238) && (myblock[1] != 113)){
		myblock[0] = (unsigned char) 238;
		myblock[1] = (unsigned char) 113;
	}

	if( (our_ptr < beginning) || (our_ptr > end)) {
	fprintf(stderr, "Error: Pointer is not part of our memory space\n");
		return;
		//Checks if the pointer is within our memory space, just returns
		//if it is not
	}

	//iterates through the memory, checking to see if the ptr that is
	//given to us is actually a pointer in our memory that is pointing
	//to a correct metadata spot

	while(index < 4094) {
		md_size = get_size(index);
		currentAddress = (unsigned long long) &myblock[index+2];
		if( currentAddress  == our_ptr ) {
			counter = 1;
			break;
		}
		index = index + (2 +md_size);
	}
	//this means that ptr is not a valid address
	//so we simply return NULL without doing anything
	if(counter != 1) {
		fprintf(stderr, "Error: Not a valid pointer in our memory space\n");
		return;
	}
	md_toggle = get_toggle(index);
	md_size = get_size(index);

	if(md_toggle == 1) {
		mk_md(index, (size_t)md_size, 0);
	}
	else {
		fprintf(stderr, "Error: Pointer is already freed\n");
		//This means that the space here is already freed, so dont do anything
		return;
	}





	//Now we need to traverse through entire array to check for
	//adjacent frees and merge them, up until the index

	while(current_index < index) {
		md_toggle = get_toggle(current_index);
		md_size = get_size(current_index);
		//check if the current index is unallocated
		if(md_toggle == 0) {
			//check if the next block is block we just freed
			if( (md_size+2+current_index) == index) {
				//add the bytes to our current md_size, which we will remake the block
				md_size = md_size + get_size(index) +2;
				//check if the block AFTER the block we just freed is not out of bounds
				if(index+get_size(index) +2  < 4094) {
					//check if the block after the block we just freed is unallocated
					if( get_toggle(index+get_size(index)+2) == 0) {
						md_size = md_size + get_size(index+get_size(index)+2) + 2;
						myblock[index+get_size(index)+2] = '\0';
						myblock[index+get_size(index)+3] = '\0';
					}
				}
				//overwrite the current_index meta data with the merged values of the freed block, and possibly the block
				//after that
				mk_md(current_index,(size_t) md_size, 0);
				myblock[index] = '\0';
				myblock[index+1] = '\0';
				return;
			}
		}
		//get the next block's index
		current_index = current_index + md_size + 2 ;
	}

	if(index+get_size(index) < 4094) {
		md_size = get_size(index);
		if( get_toggle(index+get_size(index)+2) == 0) {
			md_size = md_size + get_size(index+get_size(index)+2) + 2;
			myblock[index+get_size(index)+2] = '\0';
			myblock[index+get_size(index)+3] = '\0';
		}
		else {
			return;
		}
		mk_md(index, (size_t) md_size, 0);
	}

	return;
}
