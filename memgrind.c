#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include "mymalloc.h"

typedef struct node{
	int val;
	struct node* next;
} *node;

suseconds_t CaseA(){
	
	struct timeval start, end;
	suseconds_t ret = 0;
	
	gettimeofday(&start, 0);
	int i;
	for(i=0; i<150; i++){
		char* a = (char*) malloc(sizeof(char));
		free(a);
	}
	gettimeofday(&end, 0);
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));
	
	return ret;
}

suseconds_t CaseB(){

	struct timeval start, end;
	gettimeofday(&start, NULL);

	suseconds_t ret = 0;
	int i;
	char* b[150];	

	for(i=0; i<150; i++){
		b[i] = (char*) malloc(sizeof(char));
	}
	for(i=0; i<3; i++){
		int j;
		for(j=(i*50); j<((i+1)*50); j++){
			free(b[j]);
		}
	}
	gettimeofday(&end, 0);
	
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));

	return ret;
}

suseconds_t CaseC(){
	
	struct timeval start, end;
	gettimeofday(&start, NULL);

	int i=0;
	int count = 0;
	char* c[50] = {0};
	suseconds_t ret = 0;

	while(i!=50){
		int n=rand()%2;
		if(n==1){
			c[count] = (char*) malloc(sizeof(char));
			count++;
			i++;
		}else{
			if(c[count] == NULL){
				continue;
			}
			free(c[count]);
			i--;
			count--;
		}
	}
	while((i-1)>=0){
		free(c[i-1]);
		i--;
	}

	gettimeofday(&end, 0);
	
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));

	return ret;
}

suseconds_t CaseD(){
	
	struct timeval start, end;
	gettimeofday(&start, NULL);
	
	int i=0;
	int count=0;
	suseconds_t ret = 0;
	
	void* d[50] = {0};

	while(i!=50){
		int n = rand()%65;

		if(n%3 != 0 && n!=0){
			d[i] = malloc((size_t) n);
			i++;
			count++;
		}else{
			if(d[count] == NULL){
				continue;
			}
			free(d[count]);
			i--;
			count--;
		}
	}
	while((i-1)>=0){
		free(d[i-1]);
		i--;
	}
	
	gettimeofday(&end, 0);
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));
	return ret;
}

suseconds_t CaseE(){
	
	struct timeval start, end;
	gettimeofday(&start, NULL);
	suseconds_t ret = 0;

	int i = 0;
	node XD = (node) malloc(sizeof(struct node));
	XD->val = 0;
	node temp = XD;

	for(i=0; i<228; i++){
		if(XD == NULL) break;
		XD->next = (node) malloc(sizeof(struct node));
		XD->val = i+1;
		XD = XD->next;
	}

	XD =  temp;
	while(XD != NULL){
		free(XD);
		XD = XD->next;
	}

	gettimeofday(&end, 0);
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));

	return ret;
}

suseconds_t CaseF(){
	
	struct timeval start, end;
	gettimeofday(&start, NULL);
	
	suseconds_t ret = 0;
	void* test[2000];
	memset(test, 0, 2000);
	int i=0;
	int j=1;

	while(i<2000){
		test[i] = malloc((size_t)(j*i+1));

		if(test[i] == NULL){
			if(test[i] == NULL){
				break;
			}
		}
		int r = rand()%(i+2);
		if(j%8 == 0){
			free(test[i]);
			test[i] = NULL;
		}else if(j%5 == 0){
			free(test[r]);
			test[r] = NULL;
		}
		i++;
		j++;
	}
	i++;
	while(i<2000){
		test[i] = malloc((size_t) 2);
		if(test[i] == NULL) break;
		i++;
	}
	for(i=0; i<2000; i++){
		if(test[i] == NULL) continue;
		free(test[i]);
	}
	
	gettimeofday(&end, 0);
	ret = ret + ((end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec));

	return ret;

}

int main(int argc, char** argv){

	//if you want to uncomment a block just add a / to the
	//opener of the block comment. Remove it to comment it
	//out. Opening blocks look like /* or //* while closing
	//looks like //*/ at all times. All opening comment blocks
	//have ////////// after the opener.
	
	srand(time(NULL));
	
	int i;
	
	suseconds_t A = 0;
	for(i=0; i<100; i++){
		A += CaseA();
	}
	
	suseconds_t B = 0;	
	for(i=0; i<100; i++){
		B += CaseB();
	}

	suseconds_t C = 0;
	for(i=0; i<100; i++){
		C += CaseC();
	}

	suseconds_t D = 0;
	for(i=0; i<100; i++){
		D += CaseD();
	}
	
	suseconds_t E = 0;
	for(i=0; i<100; i++){
		E += CaseE();
	}

	suseconds_t F = 0;
	for(i=0; i<100; i++){
		F += CaseF();
	}
	printf("CaseA: %ld microseconds\n", (A/100));
	printf("CaseB: %ld microseconds\n", (B/100));
	printf("CaseC: %ld microseconds\n", (C/100));
	printf("CaseD: %ld microseconds\n", (D/100));
	printf("CaseE: %ld microseconds\n", (E/100));
	printf("CaseF: %ld microseconds\n", (F/100));
		
	return 0;
}
